package com.yue.city.msacityserver.controller;

import com.yue.city.msacityserver.service.ICityDataService;
import com.yue.city.msacityserver.vo.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 城市列表控制器
 *
 * @author yuelinbo
 * @date 2018年07月25日 18:05
 * Copyright:2018-版权所有
 */
@RestController
@RequestMapping("/cities")
public class CityController {


    @Autowired
    private ICityDataService cityDataService;

    @GetMapping
    public List<City> listCity() throws Exception {

        return cityDataService.listCity();
    }
}
