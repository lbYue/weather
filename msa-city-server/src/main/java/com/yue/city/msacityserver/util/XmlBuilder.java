package com.yue.city.msacityserver.util;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.Reader;
import java.io.StringReader;

/**
 * xml工具类
 *
 * @author yuelinbo
 * @date 2018年07月24日 11:18
 * Copyright:2018-版权所有
 */
public class XmlBuilder {


    /**
     * 将xml转为指定的pojo
     *
     * @author yuelinbo
     * @Date 2018/7/24 11:20
     */
    public static Object xmlStrToObject(Class<?> clazz, String xmlStr) throws Exception {

        Object xmlObject = null;
        Reader reader = null;

        JAXBContext context = JAXBContext.newInstance(clazz);
        //xml转为对象的接口
        Unmarshaller unmarshaller = context.createUnmarshaller();
        reader = new StringReader(xmlStr);
        xmlObject = unmarshaller.unmarshal(reader);
        if (null != reader) {
            reader.close();
        }

        return xmlObject;

    }


}
