package com.yue.city.msacityserver.service.impl;

import com.yue.city.msacityserver.service.ICityDataService;
import com.yue.city.msacityserver.util.XmlBuilder;
import com.yue.city.msacityserver.vo.City;
import com.yue.city.msacityserver.vo.CityList;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * @author yuelinbo
 * @date 2018年07月24日 13:49
 * Copyright:2018-版权所有
 */
@Service
public class CityDataServiceImpl implements ICityDataService {

    @Override
    public List<City> listCity() throws Exception {

        //读取xml文件
        Resource resource = new ClassPathResource("citylist.xml");
        BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream(), "utf-8"));
        StringBuffer sb = new StringBuffer();
        String line = "";

        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        br.close();

        //xml 转为对象
        CityList cityList = (CityList) XmlBuilder.xmlStrToObject(CityList.class, sb.toString());

        return cityList.getCityList();
    }
}
