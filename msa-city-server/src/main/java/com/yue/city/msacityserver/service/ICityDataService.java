package com.yue.city.msacityserver.service;


import com.yue.city.msacityserver.vo.City;

import java.util.List;

/**
 * 城市服务接口
 *
 * @author yuelinbo
 * @date 2018年07月24日 13:48
 * Copyright:2018-版权所有
 */
public interface ICityDataService {


    /**
     * 获取城市列表
     *
     * @author yuelinbo
     * @Date 2018/7/24 13:49
     */
    List<City> listCity() throws Exception;
}
