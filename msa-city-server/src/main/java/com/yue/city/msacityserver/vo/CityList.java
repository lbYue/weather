package com.yue.city.msacityserver.vo;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * 城市集合
 *
 * @author yuelinbo
 * @date 2018年07月24日 11:15
 * Copyright:2018-版权所有
 */
@Data
@XmlRootElement(name = "c")
@XmlAccessorType(XmlAccessType.FIELD)  //访问类型，通过字段访问
public class CityList {

    @XmlElement(name = "d")
    private List<City> cityList;
}
