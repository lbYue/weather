package com.yue.report.msareportserver.controller;

import com.yue.report.msareportserver.service.ICityDataService;
import com.yue.report.msareportserver.service.IWeatherReportService;
import com.yue.report.msareportserver.vo.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * 天气预报控制器
 *
 * @author yuelinbo
 * @date 2018年07月24日 15:53
 * Copyright:2018-版权所有
 */
@RestController
@RequestMapping("/report")
public class WeatherReportController {

    @Autowired
    private IWeatherReportService weatherReportService;

    @Autowired
    private ICityDataService cityDataService;


    @GetMapping("/cityId/{cityId}")
    public ModelAndView getReportByCityId(@PathVariable("cityId") String cityId, Model model) throws Exception {

        //TODO 城市列表
        List<City> cityList = cityDataService.listCity();

        model.addAttribute("title", "小小的天气预报");
        model.addAttribute("cityId", cityId);
        model.addAttribute("cityList", cityList);
        model.addAttribute("report", weatherReportService.getDataByCityId(cityId));

        return new ModelAndView("weather/report", "reportModel", model);
    }
}
