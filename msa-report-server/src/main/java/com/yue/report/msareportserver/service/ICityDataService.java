package com.yue.report.msareportserver.service;


import com.yue.report.msareportserver.vo.City;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 城市服务接口
 *
 * @author yuelinbo
 * @date 2018年07月24日 13:48
 * Copyright:2018-版权所有
 */
@FeignClient("msa-city-server")
public interface ICityDataService {


    /**
     * 获取城市列表
     *
     * @author yuelinbo
     * @Date 2018/7/24 13:49
     */
    @GetMapping("/cities")
    List<City> listCity() throws Exception;
}
