package com.yue.report.msareportserver.service.impl;

import com.yue.report.msareportserver.service.IWeatherDataService;
import com.yue.report.msareportserver.service.IWeatherReportService;
import com.yue.report.msareportserver.vo.Weather;
import com.yue.report.msareportserver.vo.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 天气预报接口实现类
 *
 * @author yuelinbo
 * @date 2018年07月24日 15:44
 * Copyright:2018-版权所有
 */
@Service
public class WeatherReportServiceImpl implements IWeatherReportService {

    @Autowired
    private IWeatherDataService weatherDataService;

    @Override
    public Weather getDataByCityId(String cityId) {

        //TODO 天气信息
        WeatherResponse response = weatherDataService.getDataByCityId(cityId);


        return response.getData();
    }
}
