package com.yue.report.msareportserver.service;


import com.yue.report.msareportserver.vo.Weather;

/**
 * 天气预报服务接口
 *
 * @author yuelinbo
 * @date 2018年07月24日 15:43
 * Copyright:2018-版权所有
 */
public interface IWeatherReportService {

    Weather getDataByCityId(String cityId);
}
