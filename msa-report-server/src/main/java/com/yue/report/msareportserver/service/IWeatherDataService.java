package com.yue.report.msareportserver.service;


import com.yue.report.msareportserver.vo.WeatherResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 城市服务接口
 *
 * @author yuelinbo
 * @date 2018年07月24日 13:48
 * Copyright:2018-版权所有
 */
@FeignClient("msa-data-server")
public interface IWeatherDataService {


    /**
     * 获取城市列表
     *
     * @author yuelinbo
     * @Date 2018/7/24 13:49
     */
    @GetMapping("/weather/cityId/{cityId}")
    WeatherResponse getDataByCityId(@PathVariable("cityId") String cityId);
}
