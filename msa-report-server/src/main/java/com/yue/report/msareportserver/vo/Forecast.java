package com.yue.report.msareportserver.vo;

import lombok.Data;

import java.io.Serializable;

/** 未来天气
 * @author yuelinbo
 * @date 2018年07月16日 17:49
 * Copyright:2018赛鼎科技-版权所有
 */
@Data
public class Forecast implements Serializable{


    private static final long serialVersionUID = 1506164631480756004L;

    private String date;
    private String high;
    private String fengli;
    private String low;
    private String fengxiang;
    private String type;




}
