package com.yue.data.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsaDataServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsaDataServerApplication.class, args);
    }
}
