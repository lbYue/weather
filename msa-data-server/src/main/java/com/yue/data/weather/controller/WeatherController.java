package com.yue.data.weather.controller;

import com.yue.data.weather.service.IWeatherDataService;
import com.yue.data.weather.vo.WeatherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Weather Controller.
 *
 * @author yuelinbo
 * @date 2018年07月25日 16:11
 * Copyright:2018-版权所有
 */
@RestController
@RequestMapping("/weather")
public class WeatherController {
    @Autowired
    private IWeatherDataService weatherDataService;

    @GetMapping("/cityId/{cityId}")
    public WeatherResponse getWeatherByCityId(@PathVariable("cityId") String cityId) {
        return weatherDataService.getDataByCityId(cityId);
    }
}
