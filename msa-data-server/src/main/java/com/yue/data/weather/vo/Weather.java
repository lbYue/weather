package com.yue.data.weather.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 天气信息
 *
 * @author yuelinbo
 * @date 2018年07月16日 17:45
 * Copyright:2018赛鼎科技-版权所有
 */
@Data
public class Weather implements Serializable {


    private static final long serialVersionUID = 4259604248249916690L;

    private String city;
    private String aqi;
    private String ganmao;
    private String wendu;
    private Yesterday yesterday;
    private List<Forecast> forecast;
}
