package com.yue.data.weather.vo;

import lombok.Data;

import java.io.Serializable;

/** 天气对象返回信息
 * @author yuelinbo
 * @date 2018年07月17日 09:37
 * Copyright:2018赛鼎科技-版权所有
 */
@Data
public class WeatherResponse implements Serializable {

    private static final long serialVersionUID = 8788257810377003028L;

    private Weather data;
    private Integer status;
    private String desc;
}
