package com.yue.data.weather.service;

import com.yue.data.weather.vo.WeatherResponse;

/**
 * @author yuelinbo
 * @date 2018年07月25日 16:10
 * Copyright:2018-版权所有
 */
public interface IWeatherDataService {

    /**
     * 根据城市ID查询天气数据
     *
     * @param cityId
     * @return
     */
    WeatherResponse getDataByCityId(String cityId);

}
