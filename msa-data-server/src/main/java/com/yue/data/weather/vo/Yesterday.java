package com.yue.data.weather.vo;

import lombok.Data;

import java.io.Serializable;

/** 昨日天气
 * @author yuelinbo
 * @date 2018年07月16日 17:49
 * Copyright:2018赛鼎科技-版权所有
 */
@Data
public class Yesterday implements Serializable {

    private static final long serialVersionUID = 7659999746791649826L;


    private String date;
    private String high;
    private String fx;
    private String low;
    private String fl;
    private String type;
}
