package com.yue.collection.weather.config;

import com.yue.collection.weather.job.WeatherDataSyncJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 定时器配置类
 *
 * @author yuelinbo
 * @date 2018年07月24日 09:46
 * Copyright:2018-版权所有
 */
@Configuration
public class QuartzConfiguration {

    private static final int TIME = 3600; //更新频率 秒

    //jobDetail 特定的job
    @Bean
    public JobDetail weatherDataSyncJobDetail() {

        JobDetail jobDetail = JobBuilder.newJob(WeatherDataSyncJob.class)
                .withIdentity("weatherDataSyncJob").storeDurably().build();

        return jobDetail;
    }

    //Trigger 触发器
    @Bean
    public Trigger weatherDataSyncTrigger() {

        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInSeconds(TIME).repeatForever();

        return TriggerBuilder.newTrigger().forJob(weatherDataSyncJobDetail())
                .withIdentity("weatherDataSyncTrigger").withSchedule(scheduleBuilder).build();
    }
}
