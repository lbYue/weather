package com.yue.collection.weather.job;

import com.yue.collection.weather.service.ICityDataService;
import com.yue.collection.weather.service.IWeatherDataConllectionService;
import com.yue.collection.weather.vo.City;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 天气数据同步Job
 *
 * @author yuelinbo
 * @date 2018年07月23日 17:27
 * Copyright:2018-版权所有
 */
public class WeatherDataSyncJob extends QuartzJobBean {


    private static final Logger logger = LoggerFactory.getLogger(WeatherDataSyncJob.class);


    @Autowired
    private IWeatherDataConllectionService weatherDataService;

    @Autowired
    private ICityDataService cityDataService;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {

        logger.info("Weather Data sync Job Start");

        List<City> cityList = null;
        try {
            cityList = cityDataService.listCity();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (City city1 : cityList) {
            String cityId = city1.getCityId();

            weatherDataService.syncDataByCityId(cityId);


            logger.info("weather data sync end CityId:" + cityId);
        }
    }
}
