package com.yue.collection.weather.service;

/**
 * 天气数据采集接口
 *
 * @author yuelinbo
 * @date 2018年07月25日 11:09
 * Copyright:2018-版权所有
 */
public interface IWeatherDataConllectionService {

    /**
     *  根据城市ID同步天气
     *
     * @author yuelinbo
     * @Date 2018/7/25 14:18
     */
    void syncDataByCityId(String cityId);
}
