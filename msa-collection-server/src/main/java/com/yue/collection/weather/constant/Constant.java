package com.yue.collection.weather.constant;

/**
 * 常量类
 *
 * @author yuelinbo
 * @date 2018年07月21日 14:49
 * Copyright:2018-版权所有
 */
public class Constant {

    public static final String WEATHER_ID_URL = "http://wthrcdn.etouch.cn/weather_mini?citykey=";
    public static final String WEATHER_NAME_URL = "http://wthrcdn.etouch.cn/weather_mini?city=";
    public static final long TIME_OUT=3600L;
}
