package com.yue.collection.weather.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yuelinbo
 * @date 2018年07月16日 17:00
 * Copyright:2018赛鼎科技-版权所有
 */

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "hello world";
    }
}
